void f (int a)                 // commentaires avec "//"
{
    long long int l ;          // nouveau type

    l = a * a ;

    // déclaration des variables au plus près des instructions
    bool o ;                   // nouveau type
    o = (a > 0) ? true : false ;

    int tab [a] ;              // tableaux dynamiques
    for (int i = 0 ; i < a ; i++)   // déclaration
	if (...)
	    o = true ;
}
